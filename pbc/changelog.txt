1.6
- New frigate: Lyssa
- New cruiser: Gration
- Increased stasis beam range
- Increased magnetic accelerator range bonus
- Reduced Erebus projected shield radius to 2000
- Some minor stat tweaks

1.5
- Phase inhibitor: reduces time scale bonus, max speed and raises cloak upkeep for nearby phased ships
- Multi EMP: shield pierce chance
- Magnetic accelerator active time increased to 9 seconds and it now also gives 25% bonus to energy and ballistic range
- Ionized shield reduces chance of shield piercing by 50%
- Nanite paste duration reduced to 2 seconds
- Thanatos: 2 small slots Universal->Synergy
- Nyx: 1 medium slot Energy->Universal
- Gaia: 1 large slot Energy->Synergy
- Aidos: 2 medium slots Energy->Synergy
- Pallas: 1 medium slot Energy->Hybrid
- Tethys: 2 small slots Universal->Synergy
- Thrasos torpedo: 0.75 second arm time
- Pallas shield radius increased a bit
- Slightly increased stasis beam effectiveness vs big ships
- Twig entity support for beam effects (looks like it works, but I'm not completely sure about everything)
- Flux destabilizer now also reduces hard flux dissapation fraction by 85%, adds small chance of weapon malfunction and also deals 1 dps

1.4
- New capital carrier: Erebus
- New ship system: projected shield
- Aidos system has a better chance hacking a missile (0.75->0.85)
- EMP MIRV is now significantly faster
- Increased smart charges damage, they now also deal some EMP
- Reduced Moros $ price a bit
- Flux destabilizer now costs much less flux to fire
- Slightly wider arcs for Moros rear large mounts
- New weapon: Thrasos fast torpedo (S)
- New weapon: Artemis autoblaster (M)
- New weapon: Supercharged blaster (S)
- Bugfix: Multi EMP no longer affects phased ships
- Bugfix: stasis beams now deal 1 DPS so that flux/second stat is displayed correctly
- Bugfix: created custom ionized shield AI, since fortress shield AI didn't work

1.3
- New general purpose destroyer: Theseus
- Gaia now has new needler drones instead of pd drones
- Kratos gains a bit better flux stats
- Pallas now costs significantly less, has 360 deg shield and a bit better flux stats
- Catcher drones (for Orion and Thanatos) are now much faster
- Interdiction suite now affects catcher drones owned by the ship with hullmod
- Removed one launch bay from Aidos
- Scylla has a new system: energized armour
- Fixed the "same system descriptions" issue
- PBC now offers commission

1.2
- 0.7.1a compatibility
- moved Leibethra to (11000, 11000)
- terrain for Leibethra system
- Leto personnel transport
- Significantly increased Flux Destabilizer debuff (80% more weapon flux for frigates, 30% for capitals)

1.1
- Added Scylla frigate
- Added Moirai destroyer
- Modified some stats: Aura has more flux capacity, adjusted several ships FP, Pallas has higher flux dissapation rate
- Moros now has omni shield
- Set correct quality factor for variants

1.0
- Initial release