package data.missions.pbc_test;

import com.fs.starfarer.api.combat.BattleObjectiveAPI;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;
import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;


public class MissionDefinition implements MissionDefinitionPlugin {
	private static int generatorCounter = 0;

	public void defineMission(MissionDefinitionAPI api) {

		api.initFleet(FleetSide.PLAYER, "PBC", FleetGoal.ATTACK, false);
		api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true);

		api.setFleetTagline(FleetSide.PLAYER, "PBC fleet");
		api.setFleetTagline(FleetSide.ENEMY, "Independent fleet");
		
		api.addBriefingItem("Just a battle.");
		
		FleetSide side1;
		FleetSide side2;
		if (generatorCounter++ % 2 == 0) {
			side1 = FleetSide.PLAYER;
			side2 = FleetSide.ENEMY;
		} else {
			side2 = FleetSide.PLAYER;
			side1 = FleetSide.ENEMY;
		}
		
		//pbc_erebus_cv_cs2,pbc_moros_bb_assault,pbc_thanatos_bcl_assault
		//api.addToFleet(side1, "pbc_thanatos_bcl_assault", FleetMemberType.SHIP, true);
		//api.addToFleet(side1, "pbc_moros_bb_assault", FleetMemberType.SHIP, true);
		api.addToFleet(side1, "pbc_gration_ca_assault", FleetMemberType.SHIP, true, CrewXPLevel.ELITE);
		api.addToFleet(side1, "pbc_achlys_ftr_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(side1, "pbc_hybris_ftr_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(side1, "pbc_aidos_dd_cs2", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_phobos_ca_inter", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_aether_cl_cs", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_aura_ff_assault", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_gaia_cs_cs3", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_kratos_dd_assault", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_kratos_dd_inter", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_moirai_dd_strike2", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_nyx_dd_strike", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_orion_ff_assault", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_pallas_ff_assault2", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_phobos_ca_assault", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_moirai_dd_assault2", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_scylla_ff_balanced", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_scylla_ff_assault", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_theseus_dd_balanced", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_theseus_dd_strike2", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_lyssa_ff_cs", FleetMemberType.SHIP, false);
		
		//conquest_Standard,paragon_Elite,odyssey_Balanced
		//api.addToFleet(side2, "conquest_Standard", FleetMemberType.SHIP, true);
		//api.addToFleet(side2, "paragon_Elite", FleetMemberType.SHIP, true);
		api.addToFleet(side2, "aurora_Balanced", FleetMemberType.SHIP, true, CrewXPLevel.VETERAN);
		api.addToFleet(side2, "centurion_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "cerberus_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "dominator_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "eagle_Balanced", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "enforcer_Escort", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "gryphon_FS", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "hammerhead_Balanced", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "harbinger_Strike", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "lasher_CS", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "scarab_Experimental", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "sunder_CS", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "wolf_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "brawler_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "medusa_Attack", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "tempest_Attack", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "vigilance_FS", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "mudskipper_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "condor_Support", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "kite_Raider", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "wasp_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(side2, "gladius_wing", FleetMemberType.FIGHTER_WING, false);


		// Set up the map.
		float width = 12000f;
		float height = 14500f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;

		api.addNebula(minX + width * 0.8f - 2000, minY + height * 0.3f, 2000);
		api.addNebula(minX + width * 0.8f - 2000, minY + height * 0.4f, 2000);
		api.addObjective(minX + width * 0.3f, minY + height * 0.5f, "nav_buoy");
		api.addObjective(minX + width * 0.7f, minY + height * 0.5f, "sensor_array");
	}
}






