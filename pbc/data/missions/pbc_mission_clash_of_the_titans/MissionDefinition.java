package data.missions.pbc_mission_clash_of_the_titans;

import com.fs.starfarer.api.combat.BattleObjectiveAPI;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;


public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets
		api.initFleet(FleetSide.PLAYER, "PBC", FleetGoal.ATTACK, false);
		api.initFleet(FleetSide.ENEMY, "HSS", FleetGoal.ATTACK, true);

		// Set a blurb for each fleet
		api.setFleetTagline(FleetSide.PLAYER, "PBC special battlegroup");
		api.setFleetTagline(FleetSide.ENEMY, "Tri-Tachyon raiders");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Your flagship has a system that will help to withstand Onslaught's pulse cannons");
		
		FleetSide side1 = FleetSide.PLAYER;
		FleetSide side2 = FleetSide.ENEMY;
		
		api.addToFleet(side1, "pbc_moros_bb_assault", FleetMemberType.SHIP, true);
		api.addToFleet(side1, "pbc_aether_cl_cs", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_pallas_ff_assault2", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_orion_ff_assault", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "vigilance_Standard", FleetMemberType.SHIP, false);
		
		api.addToFleet(side2, "onslaught_Standard", FleetMemberType.SHIP, true);
		api.addToFleet(side2, "monitor_Starting", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "buffalo2_FS", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "enforcer_Outdated", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "lasher_d_CS", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "cerberus_d_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "warthog_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(side2, "talon_wing", FleetMemberType.FIGHTER_WING, false);


		// Set up the map.
		float width = 10000f;
		float height = 10500f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;

		api.addPlanet(minX + width * 0.8f, minY + height * 0.8f, 300f, "jungle", 300f);
	}

}






