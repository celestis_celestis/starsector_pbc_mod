package data.missions.pbc_mission_catch_me_if_you_can;

import com.fs.starfarer.api.combat.BattleObjectiveAPI;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;


public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets
		api.initFleet(FleetSide.PLAYER, "PBC", FleetGoal.ATTACK, false);
		api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true);

		// Set a blurb for each fleet
		api.setFleetTagline(FleetSide.PLAYER, "PBC battlegroup");
		api.setFleetTagline(FleetSide.ENEMY, "Independent mining corporation defence fleet");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Use stasis beams and catcher drones to slow down fast and small enemies");
		
		FleetSide side1 = FleetSide.PLAYER;
		FleetSide side2 = FleetSide.ENEMY;
		
		api.addToFleet(side1, "pbc_phobos_ca_inter", FleetMemberType.SHIP, true);
		api.addToFleet(side1, "pbc_gaia_cs_inter", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_pallas_ff_cs", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_orion_ff_assault", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_kratos_dd_inter", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_aura_ff_assault", FleetMemberType.SHIP, false);
		
		api.addToFleet(side2, "falcon_Attack", FleetMemberType.SHIP, true);
		api.addToFleet(side2, "hammerhead_Balanced", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "sunder_d_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "hound_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "brawler_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "tempest_Attack", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "vigilance_Strike", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "wolf_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "cerberus_Standard", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "lasher_Standard", FleetMemberType.SHIP, false);


		// Set up the map.
		float width = 10000f;
		float height = 14000f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;

		api.addPlanet(minX + width * 0.8f, minY + height * 0.8f, 300f, "jungle", 300f);
	}

}






