package data.missions.pbc_mission_antiphase;

import com.fs.starfarer.api.combat.BattleObjectiveAPI;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;


public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets
		api.initFleet(FleetSide.PLAYER, "PBC", FleetGoal.ATTACK, false);
		api.initFleet(FleetSide.ENEMY, "TTS", FleetGoal.ATTACK, true);

		// Set a blurb for each fleet
		api.setFleetTagline(FleetSide.PLAYER, "PBC interception group");
		api.setFleetTagline(FleetSide.ENEMY, "Hegemony assault forces");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Use prototype phase inhibitors to push enemies out of phase");
		
		FleetSide side1 = FleetSide.PLAYER;
		FleetSide side2 = FleetSide.ENEMY;
		
		api.addToFleet(side1, "pbc_nyx_dd_strike", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_nyx_dd_balanced", FleetMemberType.SHIP, true);
		api.addToFleet(side1, "pbc_pallas_ff_assault", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "pbc_nyx_dd_strike", FleetMemberType.SHIP, false);
		api.addToFleet(side1, "vigilance_Standard", FleetMemberType.SHIP, false);
		
		api.addToFleet(side2, "afflictor_Strike", FleetMemberType.SHIP, true);
		api.addToFleet(side2, "shade_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "harbinger_Strike", FleetMemberType.SHIP, false);
		api.addToFleet(side2, "harbinger_Strike", FleetMemberType.SHIP, false);


		// Set up the map.
		float width = 10000f;
		float height = 10500f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;

		api.addPlanet(minX + width * 0.8f, minY + height * 0.8f, 300f, "jungle", 300f);
	}

}






