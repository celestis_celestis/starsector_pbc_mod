package data.scripts.plugins;

import com.fs.starfarer.api.*;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.weapons.ai.PbcSwarmChargeAI;
import data.scripts.world.pbc.*;
import exerelin.campaign.SectorManager;

/**
 * @author celestis
 */
public class PbcModPlugin extends BaseModPlugin {

    private static final boolean isExerelin;
    
    static
    {
        boolean foundExerelin;
        try
        {
            Global.getSettings().getScriptClassLoader().loadClass("data.scripts.world.ExerelinGen");
            foundExerelin = true;
        }
        catch (ClassNotFoundException ex)
        {
            foundExerelin = false;
        }
        isExerelin = foundExerelin;
    }
    
    @Override
    public void onNewGame() {
        if (isExerelin && !SectorManager.getCorvusMode())
        {
            return;
        }
        initPbc();
    }

    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        switch (missile.getProjectileSpecId()) {
            case "pbc_swarm_charge_shot":
                return new PluginPick<MissileAIPlugin>(new PbcSwarmChargeAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            default:
                return super.pickMissileAI(missile, launchingShip);
        }
    }

    private void initPbc() {
        SectorAPI sector = Global.getSector();
        new pbc_Leibethra().generate(sector);
        new pbc_Arcadia().generate(sector);
        new pbc_Eos().generate(sector);
        new pbc_Askonia().generate(sector);
    }
}
