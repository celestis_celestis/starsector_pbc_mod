package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ShipAPI;
import db.twiglib.TwigUtils;

/**
 * @author celestis
 */
public class PbcUtil {
    public static ShipAPI getRootShip(ShipAPI ship) {
        if (Global.getSettings().getModManager().isModEnabled("ztwiglib")) {
            if (TwigUtils.isMultiShip(ship)) {
                return TwigUtils.getRoot(ship);
            }
        }
        return ship;
    }
}
