package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import java.util.*;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.ViewportAPI;

/**
 *
 * @author celestis
 *
 * This script is intended to add simple timed events. May be of use in some
 * scripts, which do not have access to advance() method.
 */
public class TimedCombatEvents implements EveryFrameCombatPlugin {

    public static class EventTimer {

        public Event event;
        public float timeLeft;
        public final boolean everyFrame;
        public final boolean manualKill;

        public EventTimer(Event event, float timeLeft, boolean everyFrame, boolean manualKill) {
            this.event = event;
            this.timeLeft = timeLeft;
            this.everyFrame = everyFrame;
            this.manualKill = manualKill;
            if (manualKill && !everyFrame) {
                throw new IllegalArgumentException("manualKill event must also be everyFrame");
            }
        }
    }

    private static final ArrayList pendingEvents = new ArrayList();

    public static interface Event {

        /**
         *
         * @param dt - amount of time elapsed since last call
         * @return true if everyFrame event should continue execution, false to
         * destroy timer.
         */
        boolean action(float dt);
    }

    //Use example:
    //addEvent(new TimedCombatEvents.Event() { 
    //	public void action(float dt) {...} 
    //}, ...);
    public static EventTimer addEvent(Event e, float time, boolean everyFrame) {
        return addEvent(e, time, everyFrame, false);
    }
    
    public static EventTimer addManualKillEvent(Event e) {
        return addEvent(e, 0, true, true);
    }
    
    private static EventTimer addEvent(Event e, float time, boolean everyFrame, boolean manualKill) {
        EventTimer r = new EventTimer(e, time, everyFrame, manualKill);
        pendingEvents.add(r);
        return r;        
    }

    //plugin interface methods:
    @Override
    public void init(CombatEngineAPI engine) {
    }

    @Override
    public void advance(float amount, List events) {
        if (Global.getCombatEngine().isPaused()) {
            return;
        }
        for (int i = pendingEvents.size() - 1; i >= 0; --i) {
            EventTimer timer = (EventTimer) pendingEvents.get(i);
            timer.timeLeft -= amount;
            if (timer.manualKill) {
                if (timer.everyFrame) {
                    if (!timer.event.action(amount)) {
                        pendingEvents.remove(i);
                    }
                } //impossible to have timer not everyFrame
            } else {
                //auto kill on timer elapsed
                if (timer.timeLeft < 0) {
                    pendingEvents.remove(i);
                    timer.event.action(amount);
                } else if (timer.everyFrame) {
                    timer.event.action(amount);
                }
            }
        }
    }
    
    @Override
    public void renderInWorldCoords(ViewportAPI vapi) {
    }

    @Override
    public void renderInUICoords(ViewportAPI vapi) {
    }

}
