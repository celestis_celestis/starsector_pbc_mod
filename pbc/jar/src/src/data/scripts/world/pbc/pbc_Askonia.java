
package data.scripts.world.pbc;

import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;

/**
 * @author celestis
 */
public class pbc_Askonia {

    public void generate(SectorAPI sector) {
        StarSystemAPI system = sector.getStarSystem("Askonia");

        SectorEntityToken outpost = system.addCustomEntity(
                "pbc_askonia_outpost" /*id*/, 
                "Agelasta Outpost" /*name*/, 
                "station_side02" /*type*/, 
                "pbc" /*faction*/); 

        outpost.setCircularOrbitPointingDown(
                system.getEntityById("cruor"), 
                130 /*angle*/, 
                210 /*radius*/, 
                20 /*days*/);

        MarketAPI outpostMarket = SectorUtil.addMarket(
                "pbc" /*faction*/,
                "Agelasta Outpost" /*name*/,
                outpost /*primary entity*/,
                null /*linked entities*/,
                3 /*size*/,
                new String[]{
                    Conditions.POPULATION_3,
                    Conditions.ORBITAL_STATION,
                    Conditions.OUTPOST,
                    Conditions.ORE_REFINING_COMPLEX
                },
                new String[]{
                    Submarkets.SUBMARKET_BLACK,
                    Submarkets.SUBMARKET_OPEN,
                    Submarkets.SUBMARKET_STORAGE
                },
                0.3f /*tariff*/);
    }
}
