
package data.scripts.world.pbc;

import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;

/**
 * @author celestis
 */
public class pbc_Eos {

    public void generate(SectorAPI sector) {
        StarSystemAPI system = sector.getStarSystem("Eos Exodus");

        SectorEntityToken outpost = system.addCustomEntity(
                "pbc_arcadia_research" /*id*/, 
                "Krocylea Facility" /*name*/, 
                "pbc_research_facility" /*type*/, 
                "pbc" /*faction*/);
        outpost.setCircularOrbitPointingDown(
                system.getEntityById("phaosphoros"), 
                200 /*angle*/, 
                550 /*radius*/, 
                17 /*days*/);
        //researchFacility.setCustomDescriptionId("");

        MarketAPI outpostMarket = SectorUtil.addMarket(
                "pbc" /*faction*/,
                "Krocylea Testing Facility" /*name*/,
                outpost /*primary entity*/,
                null /*linked entities*/,
                4 /*size*/,
                new String[]{
                    Conditions.POPULATION_4,
                    Conditions.ORBITAL_STATION,
                    Conditions.FRONTIER,
                    Conditions.ANTIMATTER_FUEL_PRODUCTION,
                    Conditions.VOLATILES_COMPLEX
                },
                new String[]{
                    Submarkets.SUBMARKET_BLACK,
                    Submarkets.SUBMARKET_OPEN,
                    Submarkets.SUBMARKET_STORAGE
                },
                0.3f /*tariff*/);
    }
}

