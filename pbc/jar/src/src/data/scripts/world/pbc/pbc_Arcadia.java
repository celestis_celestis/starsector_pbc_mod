package data.scripts.world.pbc;

import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.*;

/**
 * @author celestis
 */
public class pbc_Arcadia {

    public void generate(SectorAPI sector) {
        StarSystemAPI system = sector.getStarSystem("Arcadia");

        SectorEntityToken researchFacility = system.addCustomEntity(
                "pbc_arcadia_outpost" /*id*/, 
                "Demeter Outpost" /*name*/, 
                "station_side02" /*type*/, 
                "pbc" /*faction*/);
        researchFacility.setCircularOrbitPointingDown(
                system.getEntityById("nomios"), 
                30 /*angle*/, 
                350 /*radius*/, 
                15 /*days*/);
        //researchFacility.setCustomDescriptionId("");

        MarketAPI researchMarket = SectorUtil.addMarket(
                "pbc" /*faction*/,
                "Demeter Outpost" /*name*/,
                researchFacility /*primary entity*/,
                null /*linked entities*/,
                3 /*size*/,
                new String[]{
                    Conditions.POPULATION_3,
                    Conditions.ORBITAL_STATION,
                    Conditions.OUTPOST,
                    Conditions.STEALTH_MINEFIELDS,
                    Conditions.MILITARY_BASE,
                    Conditions.VOLATILES_DEPOT
                },
                new String[]{
                    Submarkets.SUBMARKET_BLACK,
                    Submarkets.SUBMARKET_OPEN,
                    Submarkets.GENERIC_MILITARY,
                    Submarkets.SUBMARKET_STORAGE//?remove?
                },
                0.3f /*tariff*/);
    }
}
