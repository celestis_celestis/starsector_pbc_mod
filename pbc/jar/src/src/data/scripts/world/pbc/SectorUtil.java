package data.scripts.world.pbc;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.*;

/**
 * @author celestis using the code from MesoTroniK
 */
public class SectorUtil {
    public static MarketAPI addMarket(
            String factionId,
            String name,
            SectorEntityToken primaryEntity,
            SectorEntityToken[] connectedEntities,
            int size,
            String[] marketConditions,
            String[] submarkets,
            float tariff) {
        
        EconomyAPI globalEconomy = Global.getSector().getEconomy();
        String planetID = primaryEntity.getId();
        String marketID = planetID + "_market";

        MarketAPI newMarket = Global.getFactory().createMarket(marketID, name, size);
        newMarket.setFactionId(factionId);
        newMarket.setPrimaryEntity(primaryEntity);
        newMarket.setBaseSmugglingStabilityValue(0);
        newMarket.getTariff().modifyFlat("generator", tariff);

        if (submarkets != null)
        {
            for (String market : submarkets)
            {
                newMarket.addSubmarket(market);
            }
        }

        for (String condition : marketConditions)
        {
            newMarket.addCondition(condition);
        }

        if (null != connectedEntities)
        {
            for (SectorEntityToken entity : connectedEntities)
            {
                newMarket.getConnectedEntities().add(entity);
            }
        }

        globalEconomy.addMarket(newMarket);
        primaryEntity.setMarket(newMarket);
        primaryEntity.setFaction(factionId);

        if (connectedEntities != null)
        {
            for (SectorEntityToken entity : connectedEntities)
            {
                entity.setMarket(newMarket);
                entity.setFaction(factionId);
            }
        }

        return newMarket;
    }
}
