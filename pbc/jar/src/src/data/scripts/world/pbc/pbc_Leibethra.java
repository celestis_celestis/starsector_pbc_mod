package data.scripts.world.pbc;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.ids.Terrain;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import com.fs.starfarer.api.impl.campaign.terrain.MagneticFieldTerrainPlugin.MagneticFieldParams;
import com.fs.starfarer.api.util.Misc;
import java.awt.Color;

/**
 * @author celestis
 */
public class pbc_Leibethra {
    private void initFaction(SectorAPI sector) {
        SharedData.getData().getPersonBountyEventData().addParticipatingFaction("pbc");
        
        FactionAPI pbc = sector.getFaction("pbc");

        pbc.setRelationship(Factions.PIRATES, RepLevel.HOSTILE);
        pbc.setRelationship(Factions.INDEPENDENT, RepLevel.FRIENDLY);
        pbc.setRelationship(Factions.HEGEMONY, RepLevel.NEUTRAL);
        pbc.setRelationship(Factions.DIKTAT, RepLevel.SUSPICIOUS);
        pbc.setRelationship(Factions.TRITACHYON, RepLevel.HOSTILE);
        pbc.setRelationship(Factions.LUDDIC_CHURCH, RepLevel.HOSTILE);
        pbc.setRelationship(Factions.LUDDIC_PATH, RepLevel.HOSTILE);
        pbc.setRelationship("shadow_industry", RepLevel.FAVORABLE);
        pbc.setRelationship("citadeldefenders", RepLevel.NEUTRAL);
        pbc.setRelationship("exigency", RepLevel.NEUTRAL);
        pbc.setRelationship("exipirated", RepLevel.HOSTILE);
        pbc.setRelationship("templars", RepLevel.HOSTILE);
        pbc.setRelationship("mayorate", RepLevel.NEUTRAL);
        pbc.setRelationship("junk_pirates", RepLevel.HOSTILE);
        pbc.setRelationship("pack", RepLevel.NEUTRAL);
        pbc.setRelationship("syndicate_asp", RepLevel.FAVORABLE);
        pbc.setRelationship("tiandong", RepLevel.WELCOMING);
        pbc.setRelationship("interstellarimperium", RepLevel.INHOSPITABLE);
    }
    
    private void generateSystem(SectorAPI sector) {
        StarSystemAPI system = sector.createStarSystem("Leibethra");
        system.getLocation().set(11000, 11000);
        
        SectorEntityToken nebula = Misc.addNebulaFromPNG("data/campaign/terrain/leibethra_nebula.png",
                          0, 0, // center of nebula
                          system, // location to add to
                          "terrain", "nebula_blue", // "nebula_blue", // texture to use, uses xxx_map for map
                          4, 4); // number of cells in texture

        //system.setBackgroundTextureFilename("");

        PlanetAPI star = system.initStar("pbc_leibethra", "pbc_star_leibethra", 
            550f, // radius
            400, // extent of corona outside star
            5f, // solar wind burn level
            0.25f, // flare probability
            1.2f); // CR loss multiplier, good values are in the range of 1-5
        
        // magnetic field
        SectorEntityToken field = system.addTerrain(Terrain.MAGNETIC_FIELD,
        new MagneticFieldParams(500f, // terrain effect band width 
                        2000, // terrain effect middle radius
                        star, // entity that it's around
                        1750f, // visual band start
                        2250f, // visual band end
                        new Color(70, 20, 100, 40), // base color
                        0.6f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
                        new Color(70, 20, 100, 130),
                        new Color(150, 30, 120, 150), 
                        new Color(200, 50, 130, 190),
                        new Color(250, 70, 150, 240),
                        new Color(200, 80, 130, 255),
                        new Color(75, 0, 160), 
                        new Color(127, 0, 255)
                        ));
        field.setCircularOrbit(star, 0, 0, 150);
        
        PlanetAPI planet1 = system.addPlanet("pbc_ida", star, "Ida", "toxic", 135, 120, 3000 /*r*/, 80);
        planet1.getSpec().setRotation(0.2f);
        planet1.getSpec().setTilt(-0.3f);
        planet1.applySpecChanges();

        PlanetAPI planet2 = system.addPlanet("pbc_pimpleia", star, "Pimpleia", "ice_giant", 35, 220, 10500, 180);
        planet2.getSpec().setRotation(0.1f);
        planet2.getSpec().setTilt(0.4f);
        planet2.applySpecChanges();
        system.addRingBand(planet2, "misc", "rings1", 256f, 2, Color.white, 256f, 700, 20f);

        PlanetAPI planet3 = system.addPlanet("pbc_zeleia", star, "Zeleia", "frozen", 35, 220, 2800, 180);
        planet3.getSpec().setRotation(0.13f);
        planet3.getSpec().setTilt(0.2f);
        planet3.applySpecChanges();
        
        SectorEntityToken relay = system.addCustomEntity("pbc_leibethra_relay", "Leibethra Relay", "comm_relay", "pbc");
        relay.setCircularOrbit(star, 225, 7500, 400);
        
        system.addAsteroidBelt(star, 220, 6400/*r*/, 500 /*width*/, 180 /*minOrbitDays*/, 200 /*maxOrbitDays*/, Terrain .ASTEROID_BELT, null);
        system.addAsteroidBelt(planet2, 30, 950, 50, 30, 40, Terrain.ASTEROID_BELT, null);

        JumpPointAPI jumpGate = Global.getFactory().createJumpPoint("pbc_leibethra_gate", "Leibethra Gate");
        OrbitAPI gateOrbit = Global.getFactory().createCircularOrbit(star, 235, 8200/*r*/, 190);
        jumpGate.setOrbit(gateOrbit);
        jumpGate.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jumpGate);        
        
        system.autogenerateHyperspaceJumpPoints(true, true);
        
        SectorEntityToken pegasusBase = system.addCustomEntity(
                "pbc_leibethra_pegasus_base" /*id*/, 
                "Pegasus Base" /*name*/, 
                "pbc_pegasus_base" /*type*/, 
                "pbc" /*faction*/);
        pegasusBase.setCircularOrbitPointingDown(
                star, 
                205 /*angle*/, 
                6400 /*radius*/,
                190 /*days*/);
        
        SectorEntityToken pirateBase = system.addCustomEntity(
                "pbc_leibethra_pirate_base" /*id*/, 
                "Pirate Hideout" /*name*/, 
                "station_pirate_type" /*type*/, 
                Factions.PIRATES /*faction*/);
        pirateBase.setCircularOrbitPointingDown(
                planet2, 
                20 /*angle*/, 
                350 /*radius*/,
                40 /*days*/);
        
        SectorEntityToken ttBase = system.addCustomEntity(
                "pbc_leibethra_tt_base" /*id*/, 
                "Zeleia Listening Post" /*name*/, 
                "station_side04" /*type*/, 
                Factions.TRITACHYON /*faction*/);
        ttBase.setCircularOrbitPointingDown(
                planet3, 
                220 /*angle*/, 
                320 /*radius*/,
                30 /*days*/);
        
        SectorUtil.addMarket(
                "pbc" /*faction*/,
                "Pegasus Base" /*name*/,
                pegasusBase /*primary entity*/,
                null /*linked entities*/,
                6 /*size*/,
                new String[]{
                    Conditions.POPULATION_6,
                    Conditions.SPACEPORT,
                    Conditions.REGIONAL_CAPITAL,
                    Conditions.AUTOFAC_HEAVY_INDUSTRY,
                    Conditions.AUTOFAC_HEAVY_INDUSTRY,
                    Conditions.HEADQUARTERS,
                    Conditions.ORE_COMPLEX,
                    Conditions.VICE_DEMAND,
                    Conditions.UNINHABITABLE,
                    Conditions.MILITARY_BASE
                },
                new String[]{
                    Submarkets.SUBMARKET_BLACK,
                    Submarkets.SUBMARKET_OPEN,
                    Submarkets.GENERIC_MILITARY,
                    Submarkets.SUBMARKET_STORAGE
                },
                0.3f /*tariff*/);
        
        SectorUtil.addMarket(
                "pbc" /*faction*/,
                "Ida" /*name*/,
                planet1 /*primary entity*/,
                null /*linked entities*/,
                5 /*size*/,
                new String[]{
                    Conditions.POPULATION_5,
                    Conditions.DESERT,
                    Conditions.URBANIZED_POLITY,
                    Conditions.TRADE_CENTER,
                    Conditions.ORE_REFINING_COMPLEX,
                    Conditions.FRONTIER
                },
                new String[]{
                    Submarkets.SUBMARKET_BLACK,
                    Submarkets.SUBMARKET_OPEN,
                    Submarkets.SUBMARKET_STORAGE
                },
                0.3f /*tariff*/);
        
        SectorUtil.addMarket(
                Factions.PIRATES /*faction*/,
                "Pirate Hideout" /*name*/,
                pirateBase /*primary entity*/,
                null /*linked entities*/,
                2 /*size*/,
                new String[]{
                    Conditions.POPULATION_2,
                    Conditions.ORBITAL_STATION,
                    Conditions.FRONTIER,
                    Conditions.FREE_PORT,
                    Conditions.ORGANIZED_CRIME
                },
                new String[]{
                    Submarkets.SUBMARKET_BLACK,
                    Submarkets.SUBMARKET_OPEN,
                    Submarkets.SUBMARKET_STORAGE
                },
                0.3f /*tariff*/);
        
        SectorUtil.addMarket(
                Factions.TRITACHYON /*faction*/,
                "Zeleia Listening Post" /*name*/,
                ttBase /*primary entity*/,
                null /*linked entities*/,
                2 /*size*/,
                new String[]{
                    Conditions.POPULATION_3,
                    Conditions.ORBITAL_STATION,
                    Conditions.FRONTIER,
                    Conditions.OUTPOST
                },
                new String[]{
                    Submarkets.SUBMARKET_BLACK,
                    Submarkets.GENERIC_MILITARY,
                    Submarkets.SUBMARKET_OPEN,
                    Submarkets.SUBMARKET_STORAGE
                },
                0.3f /*tariff*/);
    }
    
    public void generate(SectorAPI sector) {
        initFaction(sector);
        generateSystem(sector);
    }
}
