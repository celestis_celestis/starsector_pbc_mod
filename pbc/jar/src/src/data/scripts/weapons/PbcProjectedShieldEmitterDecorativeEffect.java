package data.scripts.weapons;

import com.fs.starfarer.api.combat.*;
import java.awt.Color;

/**
 * @author celestis
 */
public class PbcProjectedShieldEmitterDecorativeEffect implements EveryFrameWeaponEffectPlugin {

    private float time = 0;
    private int spawnIndex = 0;
    private static final float[] SIZES = new float[]{100, 180, 250, 0, 0, 0};
    private static final float INTERVAL = 0.4f;

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        if (weapon.getShip().getSystem().isActive()) {
            time += amount;
            if (time > INTERVAL) {
                time -= INTERVAL;
                float size = SIZES[spawnIndex];
                if (size > 0) {
                    engine.addHitParticle(
                            weapon.getLocation(),
                            weapon.getShip().getVelocity(),
                            size,//size
                            0.6f,//brightness
                            1f, //duration
                            new Color(70, 180, 255));
                }
                spawnIndex = (spawnIndex + 1) % SIZES.length;
            }
        } else {
            time = 0;
        }
    }
}
