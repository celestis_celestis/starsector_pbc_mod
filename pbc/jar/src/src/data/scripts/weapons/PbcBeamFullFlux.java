package data.scripts.weapons;  
  
import com.fs.starfarer.api.Global;  
import com.fs.starfarer.api.combat.BeamAPI;  
import com.fs.starfarer.api.combat.BeamEffectPlugin;  
import com.fs.starfarer.api.combat.CombatEngineAPI;  
import com.fs.starfarer.api.combat.CombatEntityAPI;  
import com.fs.starfarer.api.combat.ShieldAPI;  
import com.fs.starfarer.api.combat.ShipAPI;  
import com.fs.starfarer.api.util.IntervalUtil;  
import data.scripts.plugins.PbcUtil;
  
/** 
 * 
 * @author Debido 
 * fragments derived from Xenoargh's MegaBeamDamageEffect plugin 
 *  
 * This script is intended to allow the given Beam weapon using this script to apply hard flux damage to the enemy ship if the beam hits their shield 
 * This is probably OP, so be careful with game balancing! 
 */  
  
public class PbcBeamFullFlux implements BeamEffectPlugin {  
  
    private final IntervalUtil fireInterval = new IntervalUtil(0.1f, 0.1f); //interval between applying flux  
    private static final float FLUX_MLT =  //determines how much hard flux is generated in the enemy ship. 1.0f would be 100% of weapon DPS, 0.1f would be 10%, and 10f would be 1000%  
            Global.getSettings().getFloat("pbcBeamFullFluxMlt");//3
  
    @Override  
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {  
  
        if (engine.isPaused()) {  
            return;  
        }
  
        fireInterval.advance(amount);  
  
        CombatEntityAPI targetEntity = beam.getDamageTarget();
        if (targetEntity == null || !(targetEntity instanceof ShipAPI)) return;
        ShipAPI target = PbcUtil.getRootShip((ShipAPI)targetEntity);
        //If we have a target, target is a Ship, and shields are being hit.  
        ShieldAPI shield = target.getShield();
        if (target instanceof ShipAPI && shield != null && shield.isWithinArc(beam.getTo()) && shield.isOn()) {  
            if (fireInterval.intervalElapsed()) {  
                if (beam.getBrightness() >= 1f) {  
                    ShipAPI ship = (ShipAPI) target; //cast as ship  
                    ship.getFluxTracker().increaseFlux(FLUX_MLT * beam.getWeapon().getDerivedStats().getDps() / 10f, true); //apply flux  
                }  
            }  
        }  
    }  
}  