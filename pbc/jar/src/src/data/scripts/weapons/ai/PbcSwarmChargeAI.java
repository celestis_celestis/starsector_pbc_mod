package data.scripts.weapons.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcSwarmChargeAI implements MissileAIPlugin, GuidedMissileAI {

    private static final float TARGET_SEARCH_RANGE = 2500;
    private static final float AI_SWITCH_RANGE_FACTOR = 5f;
    private static final float ORBIT_RANGE_FACTOR = 2.8f;
    private static final float ORBIT_WIDTH = 80;

    private final CombatEngineAPI engine;
    private final MissileAPI missile;
    private final ShipAPI launchingShip;
    private CombatEntityAPI target;

    private boolean isFinalRun = false;
    private boolean canOrbit = true;

    public PbcSwarmChargeAI(MissileAPI missile, ShipAPI launchingShip) {
        this.engine = Global.getCombatEngine();
        this.missile = missile;
        this.launchingShip = launchingShip;
    }

    @Override
    public void advance(float dt) {
        if (Global.getCombatEngine().isPaused() || missile.isFading() || missile.isFizzling()) {
            return;
        }
        updateTarget();
        if (target != null) {
            if (launchingShip.getSystem().isActive()) {
                updateMovementFar();
            } else {
                float dtt = MathUtils.getDistanceSquared(missile, target);
                float switchRadius = AI_SWITCH_RANGE_FACTOR * target.getCollisionRadius();
                if (dtt < switchRadius * switchRadius && !isFinalRun && canOrbit) {
                    updateMovementClose();
                } else {
                    updateMovementFar();
                }
            }
        }
        if (missile.getWeapon().getChargeLevel() < 1f || launchingShip.getFluxTracker().isOverloadedOrVenting()) {
            isFinalRun = true;
        }
    }

    private static Vector2f normalize(Vector2f a) {
        float l = a.length();
        return new Vector2f(a.x / l, a.y / l);
    }

    private static boolean isOnTheRight(Vector2f a, Vector2f b) {
        Vector2f p = new Vector2f(a.y, -a.x);
        return Vector2f.dot(p, b) > 0;
    }

    private void updateMovementClose() {
        Vector2f vel = missile.getVelocity();
        Vector2f velN = normalize(vel);
        Vector2f toTargetN = VectorUtils.getDirectionalVector(missile.getLocation(), target.getLocation());
        float dtt = MathUtils.getDistance(missile, target);
        float dot = Vector2f.dot(velN, toTargetN);
        float sin = (float) Math.sqrt(1 - dot * dot);
        float closestRadius = dtt * sin;
        float orbitRadius = ORBIT_RANGE_FACTOR * target.getCollisionRadius();
        boolean isTargetOnTheRight = isOnTheRight(velN, toTargetN);
        boolean isBackwardFacing = Vector2f.dot(velN, toTargetN) < 0;
        boolean shouldTurnRight = isBackwardFacing || closestRadius > orbitRadius;
        if (!isTargetOnTheRight) {
            shouldTurnRight = !shouldTurnRight;
        }
        if (isBackwardFacing || Math.abs(closestRadius - orbitRadius) > ORBIT_WIDTH) {
            if (shouldTurnRight) {
                missile.giveCommand(ShipCommand.TURN_RIGHT);
            } else {
                missile.giveCommand(ShipCommand.TURN_LEFT);
            }
        }
        missile.giveCommand(ShipCommand.ACCELERATE);
    }

    private void updateMovementFar() {
        Vector2f interceptPoint = AIUtils.getBestInterceptPoint(
                missile.getLocation(),
                missile.getMaxSpeed(),
                target.getLocation(),
                target.getVelocity());
        if (interceptPoint == null) {
            interceptPoint = target.getLocation();
        }
        float correctAngle = VectorUtils.getAngle(
                missile.getLocation(),
                interceptPoint);
        float aimAngle = MathUtils.getShortestRotation(missile.getFacing(), correctAngle);
        missile.giveCommand(ShipCommand.ACCELERATE);

        if (Math.abs(aimAngle) > 7) {
            if (aimAngle < 0) {
                missile.giveCommand(ShipCommand.TURN_RIGHT);
            } else {
                missile.giveCommand(ShipCommand.TURN_LEFT);
            }
        }
    }

    private void updateTarget() {
        if (target == null
                || target.getOwner() == missile.getOwner()
                || ((target instanceof ShipAPI) && !((ShipAPI) target).isAlive())
                || !engine.isEntityInPlay(target)) {
            ShipAPI shipTarget = launchingShip.getShipTarget();
            if (shipTarget != null && MathUtils.getDistanceSquared(shipTarget, missile) < TARGET_SEARCH_RANGE * TARGET_SEARCH_RANGE) {
                target = shipTarget;
            } else {
                for (ShipAPI ship : CombatUtils.getShipsWithinRange(missile.getLocation(), TARGET_SEARCH_RANGE)) {
                    if (ship.isAlive() && ship.getOwner() != launchingShip.getOwner() && engine.isEntityInPlay(ship)) {
                        if (target == null) {
                            target = ship;
                        } else {
                            //pick largest nearby ship
                            if (target instanceof ShipAPI) {
                                ShipAPI targetShip = (ShipAPI) target;
                                if (getHullSizeNumber(targetShip) < getHullSizeNumber(ship)) {
                                    target = ship;
                                }
                            }
                        }
                    }
                }
            }
            setTarget(target);
            missile.giveCommand(ShipCommand.ACCELERATE);
        }
    }

    private static int getHullSizeNumber(ShipAPI ship) {
        switch (ship.getHullSize()) {
            case FIGHTER:
                return 1;
            case FRIGATE:
                return 2;
            case DESTROYER:
                return 3;
            case CRUISER:
                return 4;
            case CAPITAL_SHIP:
                return 5;
            default:
                return 0;
        }
    }

    @Override
    public CombatEntityAPI getTarget() {
        return this.target;
    }

    @Override
    public void setTarget(CombatEntityAPI target) {
        this.target = target;
        if (target instanceof ShipAPI) {
            ShipAPI shipTarget = (ShipAPI)target;
            canOrbit = shipTarget.getHullSize() != ShipAPI.HullSize.FRIGATE && shipTarget.getHullSize() != ShipAPI.HullSize.FIGHTER;
        }
    }
}
