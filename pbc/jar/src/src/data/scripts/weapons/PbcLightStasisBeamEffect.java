package data.scripts.weapons;

import com.fs.starfarer.api.combat.ShipAPI;

/**
 * @author celestis
 */
public class PbcLightStasisBeamEffect extends PbcStasisBeamEffect {

    @Override
    protected float getDebuffMlt(ShipAPI target) {
        ShipAPI.HullSize sz = target.getHullSize();
        switch (sz) {
            case FIGHTER:
                return 0.5f;
            case FRIGATE:
                return 0.75f;
            case DESTROYER:
                return 0.8f;
            case CRUISER:
                return 0.92f;
            case CAPITAL_SHIP:
                return 0.975f;
            default:
                return 1f;
        }
    }
}
