package data.scripts.weapons;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import data.hullmods.PbcInterdictionSuite;
import data.scripts.plugins.PbcUtil;
import db.twiglib.TwigUtils;

/**
 * @author celestis
 */
public class PbcStasisBeamEffect extends PbcBeamDebuffWeaponBase {

    protected float getDebuffMlt(ShipAPI target) {
        ShipAPI.HullSize sz = target.getHullSize();
        switch (sz) {
            case FIGHTER:
                return 0.4f;
            case FRIGATE:
                return 0.6f;
            case DESTROYER:
                return 0.72f;
            case CRUISER:
                return 0.83f;
            case CAPITAL_SHIP:
                return 0.89f;
            default:
                return 1f;
        }
    }

    @Override
    protected void apply(ShipAPI ship, ShipAPI owner, String id) {
        ship = PbcUtil.getRootShip(ship);
        if (ship.isHulk()) return;
        MutableShipStatsAPI shipStats = ship.getMutableStats();
        float factor = getDebuffMlt(ship);
        final String interdictionSuiteId = "pbc_interdiction_suite";
        boolean increaseDebuff = false;
        if (owner.isDrone()) {
            if (owner.getDroneSource().getVariant().hasHullMod(interdictionSuiteId)) {
                increaseDebuff = true;
            }
        } else if (owner.getVariant().hasHullMod(interdictionSuiteId)) {
            increaseDebuff = true;
        }
        if (increaseDebuff) {
            factor *= factor;
        }
        shipStats.getMaxSpeed().modifyMult(id, factor);
        shipStats.getAcceleration().modifyMult(id, factor);
        shipStats.getDeceleration().modifyMult(id, factor);
        shipStats.getMaxTurnRate().modifyMult(id, factor);
        shipStats.getTurnAcceleration().modifyMult(id, factor);
    }

    @Override
    protected void unapply(ShipAPI ship, ShipAPI owner, String id) {
        ship = PbcUtil.getRootShip(ship);
        MutableShipStatsAPI shipStats = ship.getMutableStats();
        shipStats.getMaxSpeed().unmodify(id);
        shipStats.getAcceleration().unmodify(id);
        shipStats.getDeceleration().unmodify(id);
        shipStats.getMaxTurnRate().unmodify(id);
        shipStats.getTurnAcceleration().unmodify(id);
    }
}
