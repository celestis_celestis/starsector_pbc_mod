package data.scripts.weapons;

import com.fs.starfarer.api.combat.*;
import data.scripts.plugins.TimedCombatEvents;

/**
 * @author EnderNerdcore modified by celestis
 */
public abstract class PbcBeamDebuffWeaponBase implements BeamEffectPlugin {

    private static final float UNAPPLY_TIMEOUT = 0.5f;

    private final String id = "BeamDebuffWeaponBase_" + hashCode();
    private ShipAPI currentTarget;
    private TimedCombatEvents.EventTimer unapplyEvent;

    protected abstract void apply(ShipAPI ship, ShipAPI owner, String id);

    protected abstract void unapply(ShipAPI ship, ShipAPI owner, String id);

    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {
        final ShipAPI owner = beam.getSource();
        CombatEntityAPI target = beam.getDamageTarget();
        if (target != null && beam.getBrightness() >= 1f && target instanceof ShipAPI) {
            final ShipAPI ship = (ShipAPI) target;
            if (currentTarget == ship) {
                //reset timer not to unapply from last target
                unapplyEvent.timeLeft = UNAPPLY_TIMEOUT;
            } else {
                //hitting someone else
                currentTarget = ship;
                apply(currentTarget, owner, id);
                final PbcBeamDebuffWeaponBase self = this;
                unapplyEvent = TimedCombatEvents.addEvent(new TimedCombatEvents.Event() {

                    @Override
                    public boolean action(float dt) {
                        self.unapply(ship, owner, id);
                        return true;
                    }
                }, UNAPPLY_TIMEOUT, false);

            }
        } else /*if (target == null)*/ {
            currentTarget = null;
            unapplyEvent = null;
        }
    }
}
