package data.scripts.weapons;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.plugins.PbcUtil;

/**
 * @author celestis
 */
public class PbcFluxDestabilizerEffect extends PbcBeamDebuffWeaponBase {
    private static final float HARD_FLUX_DISSAPATION_FACTOR = 0.15f;
    private static final float WEAPON_MALFUNCTION_CHANCE = 0.02f;

    private static float getDebuffPercent(ShipAPI target) {
        ShipAPI.HullSize sz = target.getHullSize();
        switch (sz) {
            case FRIGATE:
                return 80f;
            case DESTROYER:
                return 60f;
            case CRUISER:
                return 45f;
            case CAPITAL_SHIP:
                return 30f;
            default:
                return 10f;
        }
    }

    @Override
    protected void apply(ShipAPI ship, ShipAPI owner, String id) {
        ship = PbcUtil.getRootShip(ship);
        MutableShipStatsAPI shipStats = ship.getMutableStats();
        float factor = getDebuffPercent(ship);
        shipStats.getEnergyWeaponFluxCostMod().modifyPercent(id, factor);
        shipStats.getBallisticWeaponFluxCostMod().modifyPercent(id, factor);
        shipStats.getMissileWeaponFluxCostMod().modifyPercent(id, factor);
        shipStats.getHardFluxDissipationFraction().modifyMult(id, HARD_FLUX_DISSAPATION_FACTOR);
        shipStats.getWeaponMalfunctionChance().modifyFlat(id, WEAPON_MALFUNCTION_CHANCE);
    }

    @Override
    protected void unapply(ShipAPI ship, ShipAPI owner, String id) {
        ship = PbcUtil.getRootShip(ship);
        MutableShipStatsAPI shipStats = ship.getMutableStats();
        shipStats.getEnergyWeaponFluxCostMod().unmodify(id);
        shipStats.getBallisticWeaponFluxCostMod().unmodify(id);
        shipStats.getMissileWeaponFluxCostMod().unmodify(id);
        shipStats.getHardFluxDissipationFraction().unmodify(id);
        shipStats.getWeaponMalfunctionChance().unmodify(id);
    }
}