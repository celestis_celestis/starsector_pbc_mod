package data.scripts.weapons;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;

/**
 * @author celestis
 */
public class PbcJammerEffect extends PbcBeamDebuffWeaponBase {
    private static final float RANGE_MLT = 0.75f;

    @Override
    protected void apply(ShipAPI ship, ShipAPI owner, String id) {
        MutableShipStatsAPI stats = ship.getMutableStats();
        stats.getAutofireAimAccuracy().modifyMult(id, 0.25f);
        stats.getMaxRecoilMult().modifyMult(id, 2f);
        stats.getRecoilPerShotMult().modifyMult(id, 2f);
        stats.getMissileGuidance().modifyMult(id, 0.25f);
        stats.getSightRadiusMod().modifyMult(id, 0.5f);
        stats.getEnergyWeaponRangeBonus().modifyMult(id, RANGE_MLT);
        stats.getBallisticWeaponRangeBonus().modifyMult(id, RANGE_MLT);
        stats.getMissileWeaponRangeBonus().modifyMult(id, RANGE_MLT);
    }

    @Override
    protected void unapply(ShipAPI ship, ShipAPI owner, String id) {
        MutableShipStatsAPI stats = ship.getMutableStats();
        stats.getAutofireAimAccuracy().unmodify(id);
        stats.getMaxRecoilMult().unmodify(id);
        stats.getRecoilPerShotMult().unmodify(id);
        stats.getMissileGuidance().unmodify(id);
        stats.getSightRadiusMod().unmodify(id);
        stats.getEnergyWeaponRangeBonus().unmodify(id);
        stats.getBallisticWeaponRangeBonus().unmodify(id);
        stats.getMissileWeaponRangeBonus().unmodify(id);
    }
}
