package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.plugins.*;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.plugins.TimedCombatEvents;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcPlasmaEjection extends OnceAppliedShipsystemStatsScript {

    private static class EjectionTask implements TimedCombatEvents.Event {

        private static final float DURATION = 3f;
        private final ShipEngineControllerAPI.ShipEngineAPI engine;
        private final IntervalUtil interval = new IntervalUtil(0.15f, 0.25f);
        private final ShipAPI host;
        private float life;
        private float waitDuration;
        private Vector2f firstSpawnPoint;
        private Vector2f lastSpawnPoint;

        public EjectionTask(ShipEngineControllerAPI.ShipEngineAPI engine, ShipAPI ship) {
            this.engine = engine;
            host = ship;
            life = DURATION;
            firstSpawnPoint = engine.getLocation();
            lastSpawnPoint = firstSpawnPoint;
        }

        private static int randomizeColorComponent(int v, int maxOffset) {
            v = (int) (v + maxOffset * (Math.random() - 0.5f) * 2);
            return Math.max(Math.min(255, v), 0);
        }

        @Override
        public boolean action(float dt) {
            life -= dt;
            waitDuration -= dt;
            interval.advance(dt);
            if (life <= 0 && waitDuration <= 0 || !host.isAlive()) {
                return false;
            }
            if (interval.intervalElapsed()) {
                if (life > 0) {
                    float size = engine.getContribution() * 370 * (float) Math.sqrt(Math.random() + 0.5f);
                    Color color = new Color(50, 100, 200);
                    int colorOffset = 30;
                    color = new Color(
                            randomizeColorComponent(color.getRed(), colorOffset),
                            randomizeColorComponent(color.getGreen(), colorOffset),
                            randomizeColorComponent(color.getBlue(), colorOffset), 75);
                    Global.getCombatEngine().addSmokeParticle(
                            engine.getLocation(),
                            MathUtils.getRandomPointInCircle(new Vector2f(0, 0), 12),
                            size,
                            0.15f,//opacity 
                            8f, //duration
                            color);
                    lastSpawnPoint = engine.getLocation();
                }
                waitDuration = 7f;
                Vector2f center = MathUtils.getMidpoint(firstSpawnPoint, lastSpawnPoint);
                float debuffRadius = MathUtils.getDistance(center, firstSpawnPoint);
                for (ShipAPI ship : CombatUtils.getShipsWithinRange(center, debuffRadius)) {
                    if (ship.isAlive() && ship.getOwner() != host.getOwner()) {
                        for (ShipEngineControllerAPI.ShipEngineAPI target : ship.getEngineController().getShipEngines()) {
                            target.disable();
                        }
                    }
                }
            }
            return true;
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("Ejecting plasma", false);
        }
        return null;
    }

    @Override
    protected void applyOnce(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI host = (ShipAPI) stats.getEntity();
        for (ShipEngineControllerAPI.ShipEngineAPI engine : host.getEngineController().getShipEngines()) {
            TimedCombatEvents.addManualKillEvent(new EjectionTask(engine, host));
        }
    }
}
