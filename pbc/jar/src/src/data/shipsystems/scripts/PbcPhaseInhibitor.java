package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.plugins.TimedCombatEvents;
import static data.shipsystems.scripts.PbcProjectedShield.RANGE;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcPhaseInhibitor implements ShipSystemStatsScript {

    public static final float RANGE = 800;
    private static final float TIME_SCALE = 0.5f;
    private static final float SPEED_FACTOR = 0.5f;
    private static final float CLOAK_UPKEED_FACTOR = 3f;

    private final HashSet<ShipAPI> affectedTargets = new HashSet<>();
    private final IntervalUtil tracker = new IntervalUtil(0.5f, 1.5f);
    private final CombatEngineAPI engine = Global.getCombatEngine();
    private float timestamp = engine.getTotalElapsedTime(false);
    private ShipAPI host;
    
    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float level) {
        host = (ShipAPI)stats.getEntity();
        if (state == State.ACTIVE) {
            for (ShipAPI affectedTarget : affectedTargets) {
                affectedTarget.setJitter(this, Color.red, 0.75f, 2, 50);
            }
            float t = engine.getTotalElapsedTime(false);
            float dt = t - timestamp;
            timestamp = t;
            tracker.advance(dt);
            if (tracker.intervalElapsed()) {
                unsetDebuffForTargetsOutOfRangeOrUnphased(id);
                setDebuffForTargetsInRange(id);
            }         
        }
    }
    
    private void unsetDebuffForTargetsOutOfRangeOrUnphased(String id) {
        ArrayList toRemove = new ArrayList();
        for (ShipAPI target : affectedTargets) {
            if (!MathUtils.isWithinRange(target, host, RANGE) || !target.isPhased()) {
                unsetDebuff(target, id);
                toRemove.add(target);
            }
        }
        affectedTargets.removeAll(toRemove);
    }
    
    private void setDebuffForTargetsInRange(String id) {
        for (ShipAPI ship : CombatUtils.getShipsWithinRange(host.getLocation(), RANGE)) {
            if (ship.isAlive() && ship.getOwner() != host.getOwner() && ship.isPhased() && !affectedTargets.contains(ship)) {
                affectedTargets.add(ship);
                setDebuff(ship, id);
            }
        }
    }

    private void unsetDebuffForAllAffectedTargets(String id) {
        for (ShipAPI target : affectedTargets) {
            unsetDebuff(target, id);
        }
        affectedTargets.clear();
    }
    
    private void setDebuff(ShipAPI target, String id) {
        target.getMutableStats().getTimeMult().modifyMult(id, TIME_SCALE);
        target.getMutableStats().getPhaseCloakUpkeepCostBonus().modifyMult(id, CLOAK_UPKEED_FACTOR);
        target.getMutableStats().getMaxSpeed().modifyMult(id, SPEED_FACTOR);
        //target.getMutableStats().getPhaseCloakActivationCostBonus().modifyPercent(id, 10000f);
    }
    
    private void unsetDebuff(ShipAPI target, String id) {
        target.getMutableStats().getTimeMult().unmodify(id);
        target.getMutableStats().getPhaseCloakUpkeepCostBonus().unmodify(id);
        target.getMutableStats().getMaxSpeed().unmodify(id);
        //target.getMutableStats().getPhaseCloakActivationCostBonus().unmodify(id);
    }
    
    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        unsetDebuffForAllAffectedTargets(id);
    }
    
    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("Phase inhibitor active", false);
        }
        return null;
    }
}
