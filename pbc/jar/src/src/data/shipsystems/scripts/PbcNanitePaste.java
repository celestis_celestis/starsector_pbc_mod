package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

/**
 * @author celestis
 */
public class PbcNanitePaste implements ShipSystemStatsScript {

    public static class ArmorStatistics {

        public float average;
        public float minimum;
    }

    public static ArmorStatistics getAverageStatistics(float[][] grid) {
        float minArmour = Float.MAX_VALUE;
        float avgArmour = 0;
        int count = 0;
        for (int i = 0; i < grid.length; ++i) {
            float[] row = grid[i];
            for (int j = 0; j < row.length; ++j) {
                float value = row[j];
                avgArmour += value;
                if (value < minArmour) {
                    minArmour = value;
                }
                count++;
            }
        }
        ArmorStatistics statistics = new ArmorStatistics();
        statistics.average = avgArmour / count;
        statistics.minimum = minArmour;
        return statistics;
    }

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if (state == State.IN) {
            ShipAPI ship = (ShipAPI) stats.getEntity();
            ArmorGridAPI gridApi = ship.getArmorGrid();
            //get average
            float[][] grid = gridApi.getGrid();
            ArmorStatistics statistics = getAverageStatistics(grid);
            if (statistics.average == statistics.minimum) {
                return;
            }
            float avgArmour = statistics.average;
            //adjust towards average
            for (int i = 0; i < grid.length; ++i) {
                float[] row = grid[i];
                for (int j = 0; j < row.length; ++j) {
                    float oldValue = row[j];
                    float newValue = oldValue + (avgArmour - oldValue) * effectLevel;
                    gridApi.setArmorValue(i, j, newValue);
                }
            }
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
    }

    @Override
    public StatusData getStatusData(int i, State state, float f) {
        if (i == 0) {
            return new StatusData("Distributing armour...", false);
        }
        return null;
    }
}
