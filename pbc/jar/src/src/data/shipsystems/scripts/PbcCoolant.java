package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

/**
 * @author celestis
 */
public class PbcCoolant implements ShipSystemStatsScript {

    private static final float FLUX_MULT = 0.25f;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        stats.getBallisticWeaponFluxCostMod().modifyMult(id, FLUX_MULT);
        stats.getEnergyWeaponFluxCostMod().modifyMult(id, FLUX_MULT);
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getBallisticWeaponFluxCostMod().unmodify(id);
        stats.getEnergyWeaponFluxCostMod().unmodify(id);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        return null;
    }

}
