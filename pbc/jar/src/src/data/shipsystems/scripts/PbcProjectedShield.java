package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;
import java.util.*;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;

/**
 * @author celestis
 */
public class PbcProjectedShield implements ShipSystemStatsScript {
    public static final float RANGE = 2000;
    public static final float SHIELD_ARC = 270.0535178235f;//weird number to be able to check for affected fighters in AI
    
    //per-ship collection of fighters, currently gaining shield from its system.
    private final HashSet<ShipAPI> affectedFighters = new HashSet<>();
    private final IntervalUtil tracker = new IntervalUtil(3f, 5f);
    private final CombatEngineAPI engine = Global.getCombatEngine();
    private float timestamp = engine.getTotalElapsedTime(false);
    private ShipAPI carrier;
    
    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float level) {
        carrier = (ShipAPI)stats.getEntity();
        if (state == State.ACTIVE) {
            for (ShipAPI affectedFighter : affectedFighters) {
                affectedFighter.setJitter(this, Color.white, 0.25f, 2, 40);
            }
            float t = engine.getTotalElapsedTime(false);
            float dt = t - timestamp;
            timestamp = t;
            tracker.advance(dt);
            if (tracker.intervalElapsed()) {
                //Global.getLogger(PbcProjectedShield.class).info("---PbcProjectedShield.apply(): " + id + " /" + hashCode());
                unsetShieldForFightersOutOfRange();
                setShieldForFightersInRange();
            }         
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        //Global.getLogger(PbcProjectedShield.class).info("---PbcProjectedShield.unapply(): " + id + " /" + hashCode());   
        unsetShieldForAllAffectedFighters();
    }

    @Override
    public StatusData getStatusData(int index, State state, float level) {
        return null;
    }
    
    private void unsetShieldForAllAffectedFighters() {
        for (ShipAPI fighter : affectedFighters) {
            unsetShield(fighter);
        }
        affectedFighters.clear();
    }
    
    private void unsetShieldForFightersOutOfRange() {
        ArrayList toRemove = new ArrayList();
        for (ShipAPI fighter : affectedFighters) {
            if (!MathUtils.isWithinRange(fighter, carrier, RANGE)) {
                unsetShield(fighter);
                toRemove.add(fighter);
            }
        }
        affectedFighters.removeAll(toRemove);
    }
    
    private static void setShield(ShipAPI ship) {
        ship.setShield(
                ShieldAPI.ShieldType.FRONT, 
                0,//upkeep 
                1f,//efficiency
                SHIELD_ARC);//arc
        ship.resetDefaultAI();
    }
    
    private static void unsetShield(ShipAPI ship) {
        ship.setShield(ShieldAPI.ShieldType.NONE, 0,0,0);
        ship.resetDefaultAI();
    }
    
    private void setShieldForFightersInRange() {
        for (ShipAPI ship : CombatUtils.getShipsWithinRange(carrier.getLocation(), RANGE)) {
            if (ship.isAlive() && ship.getOwner() == carrier.getOwner() && ship.isFighter() && !affectedFighters.contains(ship)) {
                ShieldAPI shield = ship.getShield();
                if (shield == null || shield.getType() == ShieldAPI.ShieldType.NONE) {
                    affectedFighters.add(ship);
                    setShield(ship);
                }
            }
        }
    }
}
