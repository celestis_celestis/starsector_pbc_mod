package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

/**
 * @author celestis
 */
public class PbcSwarmChargeToggle implements ShipSystemStatsScript {

    private boolean isActive = false;
    
    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float f) {
        if (state == State.ACTIVE && !isActive) {
            isActive = true;
            stats.getMissileMaxTurnRateBonus().modifyMult(id, 0.6f);
            stats.getMissileMaxSpeedBonus().modifyMult(id, 1.3f);
            stats.getMissileRoFMult().modifyMult(id, 2.3f);
            stats.getMissileWeaponFluxCostMod().modifyMult(id, 0.5f);
            stats.getMissileWeaponDamageMult().modifyMult(id, 1.3f);
            stats.getMissileWeaponRangeBonus().modifyMult(id, 0.3f);
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        if (isActive) {
            isActive = false;            
            stats.getMissileMaxTurnRateBonus().unmodify(id);
            stats.getMissileMaxSpeedBonus().unmodify(id);
            stats.getMissileRoFMult().unmodify(id);
            stats.getMissileWeaponFluxCostMod().unmodify(id);
            stats.getMissileWeaponDamageMult().unmodify(id);
            stats.getMissileWeaponRangeBonus().unmodify(id);
        }
    }

    @Override
    public StatusData getStatusData(int i, State state, float f) {
        return null;
    }
}
