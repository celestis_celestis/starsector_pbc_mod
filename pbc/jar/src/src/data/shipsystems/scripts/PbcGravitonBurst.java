package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcGravitonBurst extends OnceAppliedShipsystemStatsScript {

    private static final float RANGE = 1200;
    private static final float MAX_FORCE = 2400;
    private static final float PARTICLE_COUNT = 400;
    private static final float PARTICLE_SPEED = 1000;
    private static final float DURATION = 0.5f;
    private static final Color COLOR_GLOW = Color.cyan;

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {
        return null;
    }

    @Override
    protected void applyOnce(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI source = (ShipAPI) stats.getEntity();
        for (CombatEntityAPI entity : CombatUtils.getEntitiesWithinRange(source.getLocation(), RANGE)) {
            if (entity.getOwner() == source.getOwner()) {
                continue;
            }
            Vector2f toTarget = VectorUtils.getDirectionalVector(source.getLocation(), entity.getLocation());
            float len = Vector2f.sub(source.getLocation(), entity.getLocation(), null).length();
            float forceFactor = (1 - len / RANGE);
            float force = MAX_FORCE * forceFactor;
            CombatUtils.applyForce(entity, toTarget, force);
            if (entity instanceof MissileAPI) {
                MissileAPI missile = (MissileAPI) entity;
                if (Math.random() < forceFactor) {
                    missile.flameOut();
                }
            }
            if (entity instanceof ShipAPI) {
                ShipAPI ship = (ShipAPI) entity;
                for (ShipEngineControllerAPI.ShipEngineAPI e : ship.getEngineController().getShipEngines()) {
                    if (Math.random()*3 < forceFactor) {
                        e.disable();
                    }
                }
            }
        }
        CombatEngineAPI engine = Global.getCombatEngine();
        final float SIZE = 60;
        for (int i = 0; i < PARTICLE_COUNT; ++i) {
            Vector2f pos = MathUtils.getRandomPointOnCircumference(new Vector2f(0, 0), 1);
            float speed = PARTICLE_SPEED * (float) (Math.random() * 0.3 + 1);
            Vector2f vel = new Vector2f(pos.x * speed, pos.y * speed);
            Vector2f absPos = new Vector2f(pos.x + source.getLocation().x, pos.y + source.getLocation().y);
            engine.addSmoothParticle(absPos, vel, SIZE, 1, DURATION, COLOR_GLOW);
            //Vector2f smokeSpeed = new Vector2f(vel.x * SMOKE_SPEED_FACTOR, vel.y * SMOKE_SPEED_FACTOR);
            //engine.addSmokeParticle(absPos, smokeSpeed, SIZE/3, 0.1f, DURATION, COLOR_SMOKE);
        }
    }
}
