
package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

/**
 * @author celestis
 */
public class FluxCache implements ShipSystemStatsScript {

    private static final float BOOST_LEVEL = 0.3f;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {
        stats.getZeroFluxMinimumFluxLevel().modifyFlat(id, BOOST_LEVEL);
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getZeroFluxMinimumFluxLevel().unmodify(id);
    }

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {
        if (index == 0) {
            return new ShipSystemStatsScript.StatusData("" + (int)(BOOST_LEVEL*100), false);
        }
        return null;
    }
}
