package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import data.shipsystems.scripts.PbcPhaseInhibitor;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcPhaseInhibitorAI implements ShipSystemAIScript {

    private ShipSystemAPI system;
    private ShipAPI hostShip;
    private final IntervalUtil tracker = new IntervalUtil(1f, 1.5f);

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.hostShip = ship;
        this.system = system;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);

        if (tracker.intervalElapsed() && !system.isActive() && system.getCooldownRemaining() == 0) {
            float activationRange = PbcPhaseInhibitor.RANGE;
            float effectiveRange = activationRange * 0.6f;
            float activationChance = 0f;

            //Iterates through all ships on the map.
            Vector2f hostLocation = hostShip.getLocation();
            for (ShipAPI ship : CombatUtils.getShipsWithinRange(hostLocation, activationRange)) {
                //We don't care about this ship if it's disabled, so we continue.
                if (ship.isHulk() || ship.getOwner() == hostShip.getOwner()) {
                    continue;
                }
                ShipSystemAPI cloak = ship.getPhaseCloak();
                if (cloak == null || !cloak.isActive()) {
                    continue;
                }
                activationChance += 0.2f;
                float dist = MathUtils.getDistance(ship, hostShip);
                if (dist <= effectiveRange) {
                    activationChance += 0.5f;
                }
                if (hostShip.getFluxTracker().getFluxLevel() > 0.8) {
                    activationChance -= 0.4f;
                }
            }

            if (activationChance > 0f && ((float) Math.random() < activationChance)) {
                hostShip.useSystem();
            }
        }
    }
}
