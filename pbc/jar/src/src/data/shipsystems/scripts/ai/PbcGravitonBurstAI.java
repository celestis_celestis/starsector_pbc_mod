package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcGravitonBurstAI implements ShipSystemAIScript {

    private static final float MAX_RANGE = 800;
    private static final float EFFECTIVE_RANGE = 200;

    private ShipSystemAPI system;
    private ShipAPI hostShip;
    private final IntervalUtil tracker = new IntervalUtil(1f, 1.5f);

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.hostShip = ship;
        this.system = system;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);

        if (tracker.intervalElapsed() && !system.isOn() && system.getCooldownRemaining() == 0) {
            float activationChance = 0f;

            //Iterates through all entities on the map.
            Vector2f hostLocation = hostShip.getLocation();
            for (CombatEntityAPI entity : CombatUtils.getEntitiesWithinRange(hostLocation, MAX_RANGE)) {
                if (entity.getOwner() == hostShip.getOwner()) {
                    continue;
                }
                if (entity instanceof ShipAPI) {
                    ShipAPI ship = (ShipAPI) entity;
                    if (ship.isHulk() || ship.isPhased()) {
                        //We don't care about this ship if it's disabled or phased, so we continue.
                        continue;
                    }
                }
                float deltaChance = 0.1f;
                float dist = MathUtils.getDistance(entity, hostShip);
                if (dist <= EFFECTIVE_RANGE) {
                    deltaChance += 0.2f;
                }
                if (entity instanceof MissileAPI) {
                    deltaChance *= 0.6f;
                } else if (entity instanceof DamagingProjectileAPI) {
                    deltaChance *= 0.15f;
                }
                activationChance += deltaChance;
            }

            if (activationChance > 0f && ((float) Math.random() < activationChance)) {
                hostShip.useSystem();
            }
        }
    }
}
