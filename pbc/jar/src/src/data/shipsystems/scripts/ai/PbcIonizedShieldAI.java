package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcIonizedShieldAI implements ShipSystemAIScript {

    private static final float THREAT_RANGE = 900;

    private ShipSystemAPI system;
    private ShipAPI hostShip;
    private final IntervalUtil tracker = new IntervalUtil(1.5f, 3f);
    private float lastHardFlux = 0;

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.hostShip = ship;
        this.system = system;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);

        if (tracker.intervalElapsed() && !system.isOn() && system.getCooldownRemaining() == 0) {

            FluxTrackerAPI flux = hostShip.getFluxTracker();
            float newHardFlux = flux.getHardFlux() / flux.getMaxFlux();
            if (newHardFlux > lastHardFlux && hostShip.getShield().isOn()) {
                float activationChance = newHardFlux - lastHardFlux;
                //Iterates through all entities on the map.
                Vector2f hostLocation = hostShip.getLocation();
                for (CombatEntityAPI entity : CombatUtils.getEntitiesWithinRange(hostLocation, THREAT_RANGE)) {
                    if (entity.getOwner() == hostShip.getOwner()) {
                        continue;
                    }
                    if (entity instanceof ShipAPI) {
                        ShipAPI ship = (ShipAPI) entity;
                        if (ship.isHulk() || ship.isPhased()) {
                            //We don't care about this ship if it's disabled or phased, so we continue.
                            continue;
                        }
                        activationChance += 0.05f;
                        if (ship.getShipTarget() == hostShip) {
                            activationChance += 0.2f;
                        }
                    } else if (entity instanceof MissileAPI) {
                        activationChance += 0.05f;
                    } else if (entity instanceof DamagingProjectileAPI) {
                        activationChance += 0.02f;
                    }
                }

                if (Math.random() < activationChance) {
                    hostShip.useSystem();
                }

            }
            lastHardFlux = newHardFlux;

        }
    }
}
