package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.*;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcPlasmaEjectionAI implements ShipSystemAIScript {
    private static final float MAX_RANGE = 400;
    private static final float EFFECTIVE_RANGE = 100;

    private ShipSystemAPI system;
    private ShipAPI hostShip;
    private final IntervalUtil tracker = new IntervalUtil(1f, 1.5f);

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.hostShip = ship;
        this.system = system;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);
        Vector2f hostVelocity = hostShip.getVelocity();

        if (tracker.intervalElapsed() && !system.isActive() && system.getCooldownRemaining() == 0) {
            float activationChance = 0f;

            //Iterates through all ships on the map.
            Vector2f hostLocation = hostShip.getLocation();
            for (ShipAPI ship : CombatUtils.getShipsWithinRange(hostLocation, MAX_RANGE)) {
                //We don't care about this ship if it's disabled, so we continue.
                if (ship.isHulk() || ship.getOwner() == hostShip.getOwner()) {
                    continue;
                }
                activationChance += 0.2f;
                float dist = MathUtils.getDistance(ship, hostShip);
                if (dist <= EFFECTIVE_RANGE) {
                    activationChance += 0.5f;
                }
                Vector2f targetVelocity = ship.getVelocity();
                if (Vector2f.dot(hostVelocity, targetVelocity) < 0) {
                    activationChance = Math.max(0, activationChance - 0.6f);
                }
            }

            if (activationChance > 0f && ((float) Math.random() < activationChance)) {
                hostShip.useSystem();
            }
        }
    }
}
