package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import data.shipsystems.scripts.PbcProjectedShield;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcProjectedShieldAI implements ShipSystemAIScript {

    private ShipSystemAPI system;
    private ShipAPI hostShip;
    private final IntervalUtil tracker = new IntervalUtil(5f, 9f);
    private float activeTime = 0;
    private static final float MAX_ACTIVE_TIME = 22;
    private float hostDps;

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.hostShip = ship;
        this.system = system;
        hostDps = getDps(hostShip);
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);
        if (system.isOn()) {
            activeTime += amount;
        }
        if (tracker.intervalElapsed()) {
            if (activeTime > MAX_ACTIVE_TIME
                    || hostShip.getFluxTracker().getFluxLevel() > 0.75f
                    || getNeabyEnemyMissilesCount() >= 5) {
                //Global.getLogger(PbcProjectedShieldAI.class).info("---forced shutdown");
                setSystemEnabled(false);
            } else {
                float fightersDps = getFightersDps();
                //Global.getLogger(PbcProjectedShieldAI.class).info("---host/fighters DPS = " + hostDps + "/" + fightersDps);
                if (hostDps < fightersDps) {//TODO: handle number of missiles nearby
                    setSystemEnabled(true);
                } else {
                    setSystemEnabled(false);
                }
            }
        }
    }

    private int getNeabyEnemyMissilesCount() {
        int count = 0;
        for (MissileAPI missile : CombatUtils.getMissilesWithinRange(hostShip.getLocation(), 1000)) {
            if (missile.getOwner() != hostShip.getOwner()) {
                count++;
            }
        }
        return count;
    }

    private float getFightersDps() {
        float sum = 0;
        for (ShipAPI ship : CombatUtils.getShipsWithinRange(hostShip.getLocation(), PbcProjectedShield.RANGE)) {
            if (ship.isAlive() && ship.getOwner() == hostShip.getOwner() && ship.isFighter()) {
                ShieldAPI shield = ship.getShield();
                if (shield == null || shield.getType() == ShieldAPI.ShieldType.NONE || shield.getArc() == PbcProjectedShield.SHIELD_ARC) {
                    sum += getDps(ship);
                }
            }
        }
        return sum;
    }

    private static float getDps(ShipAPI ship) {
        float sum = 0;
        for (WeaponAPI wpn : ship.getAllWeapons()) {
            sum += wpn.getDerivedStats().getSustainedDps();
        }
        return sum;
    }

    private void setSystemEnabled(boolean enabled) {
        if (system.isOn() != enabled) {
            //Global.getLogger(PbcProjectedShieldAI.class).info("---changing active state: " + enabled);
            hostShip.useSystem();
            if (!enabled) {
                activeTime = 0;
            }
        }
    }
}
