package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShieldAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcSwarmChargeToggleAI implements ShipSystemAIScript {

    private ShipSystemAPI system;
    private ShipAPI hostShip;
    private final IntervalUtil tracker = new IntervalUtil(1.5f, 3f);

    @Override
    public void init(ShipAPI hostShip, ShipSystemAPI system, ShipwideAIFlags saif, CombatEngineAPI ceapi) {
        this.system = system;
        this.hostShip = hostShip;
    }

    @Override
    public void advance(float dt, Vector2f vctrf, Vector2f vctrf1, ShipAPI target) {
        tracker.advance(dt);
        if (tracker.intervalElapsed()) {
            if (target == null) {
                return;
            }
            ShieldAPI shield = target.getShield();
            boolean shouldUseSystem;
            if (shield == null) {
                shouldUseSystem = true;
            } else {
                shouldUseSystem = target.getFluxTracker().isOverloadedOrVenting()
                        || shield.getType() == ShieldAPI.ShieldType.NONE
                        || (shield.getType() == ShieldAPI.ShieldType.FRONT
                        && !shield.isWithinArc(hostShip.getLocation()));
            }
            setSystemActive(shouldUseSystem);
        }
    }

    private void setSystemActive(boolean isActive) {
        if (system.isOn() != isActive) {
            hostShip.useSystem();
        }
    }
}
