package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import data.shipsystems.scripts.PbcNanitePaste;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcNanitePasteAI implements ShipSystemAIScript {
    private final IntervalUtil tracker = new IntervalUtil(3f, 5f);
    private ShipSystemAPI system;
    private ShipAPI hostShip;

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.hostShip = ship;
        this.system = system;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);

        if (tracker.intervalElapsed() && !system.isActive() && system.getCooldownRemaining() == 0 && !system.isOutOfAmmo()) {
            if (hostShip.getFluxTracker().getFluxLevel() > 0.5f) return;
            ArmorGridAPI gridApi = hostShip.getArmorGrid();
            float[][] grid = gridApi.getGrid();
            PbcNanitePaste.ArmorStatistics statistics = PbcNanitePaste.getAverageStatistics(grid);
            float avgLevel = statistics.average / gridApi.getMaxArmorInCell();
            float minLevel = statistics.minimum / gridApi.getMaxArmorInCell();
            if (minLevel < 0.5 && (avgLevel - minLevel) > 0.15) {
                hostShip.useSystem();
            }
        }
    }
}
