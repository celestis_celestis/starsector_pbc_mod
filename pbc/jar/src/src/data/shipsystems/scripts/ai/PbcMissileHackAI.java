package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcMissileHackAI implements ShipSystemAIScript {

    private static final float MAX_RANGE = 1100;

    private ShipSystemAPI system;
    private ShipAPI hostShip;
    private final IntervalUtil tracker = new IntervalUtil(1f, 1.5f);

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.hostShip = ship;
        this.system = system;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);

        if (tracker.intervalElapsed() && !system.isActive() && system.getCooldownRemaining() == 0) {
            float activationChance = 0f;

            //Iterates through all ships on the map.
            Vector2f hostLocation = hostShip.getLocation();
            for (MissileAPI missile : CombatUtils.getMissilesWithinRange(hostLocation, amount)) {
                //We don't care about this ship if it's disabled, so we continue.
                if (missile.getOwner() == hostShip.getOwner()) {
                    continue;
                }
                activationChance += 0.2f;
            }

            if (activationChance > 0f && ((float) Math.random() < activationChance)) {
                hostShip.useSystem();
            }
        }
    }
}

