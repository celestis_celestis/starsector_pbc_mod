package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.*;
import data.shipsystems.scripts.PbcMultiEmp;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcMultiEmpAI implements ShipSystemAIScript {

    private ShipSystemAPI system;
    private ShipAPI hostShip;
    private final IntervalUtil tracker = new IntervalUtil(1f, 1.5f);

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.hostShip = ship;
        this.system = system;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            float activationRange = PbcMultiEmp.MAX_RANGE;
            float effectiveRange = activationRange * 0.7f;
            float activationChance = 0f;

            //Iterates through all ships on the map.
            Vector2f hostLocation = hostShip.getLocation();
            for (CombatEntityAPI entity : CombatUtils.getEntitiesWithinRange(hostLocation, activationRange)) {
                if (entity instanceof ShipAPI) {
                    ShipAPI ship = (ShipAPI)entity;
                    //We don't care about this ship if it's disabled, so we continue.
                    if (ship.isHulk() || ship.getOwner() == hostShip.getOwner() || ship.isPhased()) {
                        continue;
                    }
                    activationChance += 0.2f;
                    ShieldAPI shield = ship.getShield();
                    if (shield == null || shield.isOff() || !shield.isWithinArc(hostLocation)) {
                        activationChance += 0.4f;
                    }
                    float dist = MathUtils.getDistance(ship, hostShip);
                    if (dist <= effectiveRange) {
                        activationChance += 0.5f;
                    }
                    if (hostShip.getFluxTracker().getFluxLevel() > 0.6) {
                        activationChance -= 0.35f;
                    }
                }

                if (activationChance > 0f && !system.isActive() && system.getCooldownRemaining() == 0 && ((float) Math.random() < activationChance)) {
                    hostShip.useSystem();
                }
            }
        }
    }
}
