package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import data.shipsystems.scripts.PbcEnergizedArmour;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcEnergizedArmourAI implements ShipSystemAIScript {

    private final IntervalUtil tracker = new IntervalUtil(2f, 3.5f);
    private ShipSystemAPI system;
    private ShipAPI hostShip;

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.hostShip = ship;
        this.system = system;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);

        if (tracker.intervalElapsed() && !system.isActive() && system.getCooldownRemaining() == 0) {
            float rate = PbcEnergizedArmour.getDamageReductionPercent(hostShip) / PbcEnergizedArmour.ARMOUR_DMG_PERC_MAX;
            if (rate < 0.3 && Math.random() < 0.5f) {
                return;
            }

            float threatRate = 0;
            Vector2f hostLocation = hostShip.getLocation();
            for (CombatEntityAPI entity : CombatUtils.getEntitiesWithinRange(hostLocation, 600f)) {
                if (entity.getOwner() == hostShip.getOwner()) continue;
                if (entity instanceof ShipAPI) {
                    ShipAPI ship = (ShipAPI) entity;
                    if (ship.isHulk() || ship.getFluxTracker().isOverloadedOrVenting()) {
                        continue;
                    }
                    float threatRateFromShip = 0.05f;
                    switch (ship.getHullSize()) {
                        case CAPITAL_SHIP:
                            threatRateFromShip = 0.4f;
                            break;
                        case CRUISER:
                            threatRateFromShip = 0.3f;
                            break;
                        case DESTROYER:
                            threatRateFromShip = 0.2f;
                            break;
                        case FRIGATE:
                            threatRateFromShip = 0.08f;
                            break;
                    }
                    if (ship.getShipTarget() == hostShip) {
                        threatRateFromShip = (float)Math.sqrt(threatRateFromShip);
                    }
                    threatRate += threatRateFromShip;
                } else if (entity instanceof DamagingProjectileAPI) {
                    threatRate += 0.04f;
                }
            }
            if (threatRate > 0.5 && Math.random() < threatRate * rate) {
                hostShip.useSystem();
            }
        }
    }
}
