package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import java.awt.Color;
import org.lazywizard.lazylib.combat.CombatUtils;

/**
 * @author celestis
 */
public class PbcMissileHack extends OnceAppliedShipsystemStatsScript {

    private static final float RANGE = 1100;
    private static final float HACK_PROBABILITY = 0.85f;

    @Override
    protected void applyOnce(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI host = (ShipAPI) stats.getEntity();
        CombatEngineAPI engine = Global.getCombatEngine();
        for (MissileAPI missile : CombatUtils.getMissilesWithinRange(host.getLocation(), RANGE)) {
            if (Math.random() < HACK_PROBABILITY) {
                //missile.setOwner(host.getOwner());
                //missile.setSource(host);
                final float arcWidth = 2f;
                engine.spawnEmpArc(
                        host, 
                        host.getLocation(), 
                        host, 
                        missile, 
                        DamageType.KINETIC, 
                        0, 
                        0, 
                        RANGE, 
                        null, 
                        arcWidth,
                        Color.orange, 
                        Color.red);
                missile.flameOut();
            }
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        return null;
    }

}
