package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import java.util.HashSet;

/**
 *
 * @author celestis
 */
public abstract class OnceAppliedShipsystemStatsScript extends BaseShipSystemScript {

    private static final HashSet<MutableShipStatsAPI> activeSystems = new HashSet<>();

    protected abstract void applyOnce(MutableShipStatsAPI stats, String id, State state, float effectLevel);

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if (state == State.ACTIVE) {
            if (activeSystems.contains(stats)) {
                return;
            }
            activeSystems.add(stats);
            applyOnce(stats, id, state, effectLevel);
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        activeSystems.remove(stats);
    }
}
