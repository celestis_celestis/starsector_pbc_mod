package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import java.awt.Color;

/**
 * @author celestis
 */
public class PbcEnergizedArmour  implements ShipSystemStatsScript {
    public static final float ARMOUR_DMG_PERC_MAX = 85;
    public static final float ARMOUR_DMG_PERC_BASE = 50;
    
    //returns POSITIVE value
    public static float getDamageReductionPercent(ShipAPI ship) {
        float fluxLevel = ship.getFluxTracker().getFluxLevel();
        float dmgReduction = ARMOUR_DMG_PERC_BASE;
        if (fluxLevel > 0.5) {
            float factor = (fluxLevel - 0.5f)*2;
            dmgReduction = ARMOUR_DMG_PERC_BASE + factor*(ARMOUR_DMG_PERC_MAX - ARMOUR_DMG_PERC_BASE);
        }
        return dmgReduction;
    }

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float f) {
        ShipAPI ship = (ShipAPI)stats.getEntity();
        float buff = -getDamageReductionPercent(ship);
        stats.getArmorDamageTakenMult().modifyPercent(id, buff);
        ship.setJitter(this, Color.red, 0.1f, 2, 10f);/*java.awt.Color color, float intensity, int copies, float range*/
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getArmorDamageTakenMult().unmodify(id);
    }

    @Override
    public StatusData getStatusData(int i, State state, float f) {
        return null;
    }
}
