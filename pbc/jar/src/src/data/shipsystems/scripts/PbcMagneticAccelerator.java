
package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

/**
 * @author celestis
 */
public class PbcMagneticAccelerator implements ShipSystemStatsScript {

    private static final int PROJ_SPEED_PERC = 75;
    private static final int RANGE_PERC = 50;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {
        stats.getProjectileSpeedMult().modifyPercent(id, PROJ_SPEED_PERC);
        stats.getEnergyWeaponRangeBonus().modifyPercent(id, RANGE_PERC);
        stats.getBallisticWeaponRangeBonus().modifyPercent(id, RANGE_PERC);
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getProjectileSpeedMult().unmodify(id);
        stats.getEnergyWeaponRangeBonus().unmodify(id);
        stats.getBallisticWeaponRangeBonus().unmodify(id);
    }

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {
        if (index == 0) {
            return new ShipSystemStatsScript.StatusData("" + PROJ_SPEED_PERC, false);
        }
        return null;
    }
}

