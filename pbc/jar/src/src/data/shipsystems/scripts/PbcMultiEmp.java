package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.plugins.*;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.plugins.TimedCombatEvents;
import java.awt.Color;
import java.util.*;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * @author celestis
 */
public class PbcMultiEmp extends OnceAppliedShipsystemStatsScript {

    private static final float DURATION = 3;
    public static final float MAX_RANGE = 700;

    private static class MultiEmpTask implements TimedCombatEvents.Event {

        private final WeaponSlotAPI[] slots;
        private final ShipAPI ship;
        private final IntervalUtil interval = new IntervalUtil(0.1f, 0.3f);
        private static final int MAX_ARCS = 60;

        private final float duration;
        private final int totalArcsCount;
        private int arcsSpawned;
        private float life;

        public MultiEmpTask(ShipAPI ship, float duration) {
            this.duration = duration;
            life = duration;
            this.ship = ship;
            slots = getEmpSlots();
            totalArcsCount = (int) Math.floor((0.75f + Math.random()*0.25f) * MAX_ARCS);
        }

        private WeaponSlotAPI[] getEmpSlots() {
            ArrayList<WeaponSlotAPI> res = new ArrayList<>();
            List<WeaponSlotAPI> slotList = ship.getHullSpec().getAllWeaponSlotsCopy();
            for (WeaponSlotAPI slot : slotList) {
                if (slot.isSystemSlot()) {
                    res.add(slot);
                }
            }
            return res.toArray(new WeaponSlotAPI[res.size()]);
        }

        private void emitArcs(Vector2f source, int count) {
            List<CombatEntityAPI> filteredEntities = new ArrayList<>();
            List<CombatEntityAPI> entities = CombatUtils.getEntitiesWithinRange(source, MAX_RANGE);
            for (CombatEntityAPI e : entities) {
                if (e.getOwner() != ship.getOwner()) {
                    if (e instanceof MissileAPI) {                        
                        filteredEntities.add(e);
                    } else if  (e instanceof ShipAPI) {
                        ShipAPI ship = (ShipAPI)e;
                        if (ship.isAlive() && !ship.isPhased()) {               
                            filteredEntities.add(e);                            
                        }
                    }
                }
            }
            int size = filteredEntities.size();
            if (size > 0) {
                for (int i = 0; i < count; ++i) {
                    CombatEntityAPI target = filteredEntities.get((int) Math.floor(Math.random() * size));
                    float pierceChance = 0;
                    if (target instanceof ShipAPI) {
                        ShipAPI targetShip = (ShipAPI)target;
                        pierceChance = targetShip.getHardFluxLevel() - 0.1f;
                        pierceChance *= targetShip.getMutableStats().getDynamic().getValue(Stats.SHIELD_PIERCED_MULT);
                    }
                    
                    float damAmount = 20;
                    float empAmount = 350;
                    float thickness = 10;
                    
                    if (Math.random() < pierceChance) {
                        Global.getCombatEngine().spawnEmpArcPierceShields(
                                ship, //damageSource
                                source, //point
                                ship, //pointAnchor
                                target, //empTargetEntity
                                DamageType.ENERGY, //damageType
                                damAmount, //damAmount
                                empAmount, //empDamAmount
                                MAX_RANGE, //maxRange
                                null, //impactSoundId
                                thickness, //thickness
                                Color.white, //fringe
                                Color.blue /*core*/);
                    } else {
                        Global.getCombatEngine().spawnEmpArc(
                                ship, //damageSource
                                source, //point
                                ship, //pointAnchor
                                target, //empTargetEntity
                                DamageType.ENERGY, //damageType
                                damAmount, //damAmount
                                empAmount, //empDamAmount
                                MAX_RANGE, //maxRange
                                null, //impactSoundId
                                thickness, //thickness
                                Color.white, //fringe
                                Color.blue /*core*/);
                    }
                }
            }
            arcsSpawned += count;
        }

        @Override
        public boolean action(float dt) {
            life -= dt;
            if (life <= 0 || !ship.isAlive()) {
                return false;
            }
            interval.advance(dt);
            if (interval.intervalElapsed()) {
                float entimatedArcsCount = (1 - life / duration) * totalArcsCount;
                int arcsToSpawn = (int) (entimatedArcsCount - arcsSpawned);
                for (WeaponSlotAPI slot : slots) {
                    Vector2f pos = slot.computePosition(ship);
                    int arcsToSpawnForSlot = (int) ((Math.random() * arcsToSpawn) / slots.length);
                    emitArcs(pos, arcsToSpawnForSlot);
                }
            }
            return true;
        }
    }


    @Override
    protected void applyOnce(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        CombatEntityAPI entity = stats.getEntity();
        if (entity instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) entity;
            TimedCombatEvents.addManualKillEvent(new MultiEmpTask(ship, DURATION));
        }
    }

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {
        if (index == 0) {
            return new ShipSystemStatsScript.StatusData("EMP emitters active", false);
        }
        return null;
    }
}
