package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

/**
 * @author celestis
 */
public class PbcIonizedShield  implements ShipSystemStatsScript {
    private static final float PIERCE_MULT = 0.5f;
    private static final int ENERGY_DMG_PERC = -75;
    private static final int KINETIC_DMG_PERC = -50;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {
        stats.getEnergyShieldDamageTakenMult().modifyPercent(id, ENERGY_DMG_PERC);
        stats.getKineticShieldDamageTakenMult().modifyPercent(id, KINETIC_DMG_PERC);
        stats.getDynamic().getStat(Stats.SHIELD_PIERCED_MULT).modifyMult(id, PIERCE_MULT);		
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getEnergyShieldDamageTakenMult().unmodify(id);
        stats.getKineticShieldDamageTakenMult().unmodify(id);
        stats.getDynamic().getStat(Stats.SHIELD_PIERCED_MULT).unmodify(id);
    }

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {
        if (index == 0) return new StatusData("" + ENERGY_DMG_PERC, false);
        if (index == 1) return new StatusData("" + KINETIC_DMG_PERC, false);
        return null;
    }

}
