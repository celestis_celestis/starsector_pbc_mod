package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;

/**
 * @author celestis
 */
public class PbcOmniArmour extends BaseHullMod {
    @Override
    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        return null;
    }

    @Override
    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        //stats.getKineticDamageTakenMult().modifyMult(id, 2);
        stats.getHighExplosiveDamageTakenMult().modifyMult(id, 0.5f);
        stats.getEmpDamageTakenMult().modifyMult(id, 0.25f);
        stats.getEngineDamageTakenMult().modifyMult(id, 0.5f);
        stats.getBeamDamageTakenMult().modifyMult(id, 0.75f);
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return false;
    }
}
