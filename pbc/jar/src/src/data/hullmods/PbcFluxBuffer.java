package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;

/**
 * @author celestis
 */
public class PbcFluxBuffer extends BaseHullMod {
    @Override
    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        return null;
    }

    @Override
    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getZeroFluxMinimumFluxLevel().modifyFlat(id, 0.05f);
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return false;
    }
}
